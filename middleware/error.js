module.exports = (err, req, res, next) => {
  switch (err.code) {
    case 'ENOTAUTHORIZED':
      req.log.warn(err);
      res.status(401).send({ message: err.message }).end();
      break;
    default:
      req.log.error(err);
      res.status(500).send({ message: err.message }).end();
      break;
  }
};

