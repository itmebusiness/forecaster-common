const { AuthGateway } = require('../lib/auth');

const getParsedSubscription = (value) => ({
    value,
    dataset: value,
    trial: false
});

function getParsedSubscriptions(subscriptions) {
    return subscriptions ?
        subscriptions
            .map(getParsedSubscription)
             : [];
}

const getAccessToken = authorization => authorization ? authorization.substr(7) : undefined;

function getAccountObject(auth, options) {
    return auth[`${options.auth.namespace}/account`];
}

function getParsedAuthObject(auth, options) {
    const account = getAccountObject(auth, options);
    const subscriptions = getParsedSubscriptions(account.subscriptions);
    delete account.subscriptions;
    return {
        account,
        subscriptions,
        sub: auth.sub
    }
}

const authorizer = (options) => {
    return (req, res, next) => {
        const token = getAccessToken(req.get('Authorization'));
        authorizer.AuthGateway(options.auth)
            .auth(token)
            .then(auth => {
                req.auth = getParsedAuthObject(auth, options);
                req.token = token;
                if(!req.auth.subscriptions.length) {
                    throw new Error('No subscriptions found');
                }
            })
            .catch(cause => {
                const err = new Error(`Not authorized: ${cause.message}`);
                err.code = 'ENOTAUTHORIZED';
                throw err;
            })
            .then(next)
            .catch(next);
    };
};

authorizer.AuthGateway = AuthGateway;

module.exports = authorizer;
