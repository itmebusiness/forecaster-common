const os = require('os');
const expressBunyanLogger = require('express-bunyan-logger');
const logger = require('../lib/logger');

const format = value => value.toString();
const date = new Date();
const yyyy = date.getUTCFullYear();
const mm = format(date.getUTCMonth() + 1);
const dd = format(date.getUTCDate());



const expressLogger = options => {
    const loggerConfig = {
        level: options.logLevel,
        cloudwatch: options.cloudwatch && {
            ...options.cloudwatch,
            logStreamName: `${yyyy}/${mm}/${dd}/${os.hostname()}`
        }
    };

    return expressBunyanLogger({
        logger: logger(loggerConfig),
        includesFn: (req, res) => ({
            user: req.auth && req.auth.sub
        }),
        excludes: [
            'req', 'res', 'user-agent', 'body', 'response-hrtime',
            'req-headers', 'res-headers', 'incoming'
        ]
    });
};

module.exports = expressLogger;
