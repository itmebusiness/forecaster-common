const Handlebars = require('handlebars');
const hbsHelpers = require('./helpers')(Handlebars);

class SqlTemplate {
  constructor(raw, pageSize) {
    this.raw = raw;
    this.pageSize = pageSize;
    this.templateContext = this.initTemplateContext();
    this._sourceTemplateContent = null;
    this._subTemplateContent = null;
  }

  initTemplateContext() {
    const interval = this.raw.range.interval || 'yearly';

    return Object.assign(this.raw, {
      standardTable: `${this.capitalize(interval)}Forecaster_mod`,
      complexTable: `${this.capitalize(interval)}ForecasterComplex_mod`,
      pageSize: this.pageSize
    });
  }

  initPartials() {
    Handlebars.registerPartial('subSelect', this.subSelectTemplateContent);
  }

  get sourceTemplateContent() {
    return this._sourceTemplateContent;
  }

  set sourceTemplateContent(val) {
    this._sourceTemplateContent = val;
  }

  get subSelectTemplateContent() {
    return this._subTemplateContent;
  }

  set subSelectTemplateContent(val) {
    this._subTemplateContent = val;
    this.initPartials();
  }

  get subSelectTemplate() {
    return Handlebars.compile(subSelectTemplate);
  }

  get template() {
    return Handlebars.compile(this.sourceTemplateContent);
  }

  get query() {
    return this.template(this.templateContext);
  }

  capitalize(word) {
    return word.charAt(0).toUpperCase() + word.slice(1);
  }
}

module.exports = SqlTemplate;