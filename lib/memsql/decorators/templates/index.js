module.exports.sourceTemplate = (
  `SELECT COMBINEDRESULT.* FROM (
    {{> subSelect table=standardTable}}
    UNION ALL
    {{> subSelect table=complexTable}}
  ) AS COMBINEDRESULT
  {{{sort @root}}}
  {{{limit @root}}}`
);

module.exports.subSelectTemplate = (
  `SELECT {{{columns @root}}},
  {{{pivot @root "SUM" "value"}}},
  {{{pivot @root "AVG" "sourcevalue"}}},
  {{{groupConcat @root "ID"}}},
  {{{groupConcat @root "recordstatus"}}},
  {{{groupConcat @root "lastupdateddate"}}}
  FROM {{table}}
  {{{where @root table true true}}}
  {{{groupBy @root}}}`
);

module.exports.filterWhereTemplateCount = (
  `SELECT COUNT(*) as count FROM (
    {{> subSelect table=standardTable}}
    UNION ALL
    {{> subSelect table=complexTable}}
  ) AS COMBINEDRESULT
  {{{combinedWhere @root}}}`
);

module.exports.filterWhereTemplate = (
  `SELECT COMBINEDRESULT.* FROM (
    {{> subSelect table=standardTable}}
    UNION ALL
    {{> subSelect table=complexTable}}
  ) AS COMBINEDRESULT
  {{{combinedWhere @root}}}
  {{{sort @root}}}
  {{{limit @root}}}`
);

module.exports.distinctColumnTemplate = (
  `SELECT DISTINCT COMBINEDRESULT.{{column}} FROM (
    {{> subSelect table=standardTable}}
    UNION ALL
    {{> subSelect table=complexTable}}
  ) AS COMBINEDRESULT
  WHERE COMBINEDRESULT.{{column}} IS NOT NULL
  AND COMBINEDRESULT.{{column}} != ""
  ORDER BY {{column}} ASC`
);

module.exports.sourceTemplateCount = (
  `SELECT COUNT(*) as count FROM (
    {{> subSelect table=standardTable}}
    UNION ALL
    {{> subSelect table=complexTable}}
  ) AS COMBINEDRESULT`
);
