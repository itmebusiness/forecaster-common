const headerGenerator = require('../../headers');
const helpers = module.exports;
const _forEach = require('lodash/forEach');
const _difference = require('lodash/difference');
const mappings = require('../../mappings.json');
const cloneDeep = require('lodash/cloneDeep');

helpers.distinct = (ctx) => {
  return `DISTINCT(${ctx.distinct}) as dst`;
};

const currencyMap = {
  "valueeur": 'currencyeur',
  "valuelocal": 'currencylocal',
  "valuereported": 'currencyreported',
  "usd": 'currency',
}

helpers.columns = (ctx) => {
  if(ctx.valueField !== 'usd' && ctx.valueField) {
    return ctx.columnKeys.map(el => {
      if(el == 'currency') {
        return `${currencyMap[ctx.valueField]} AS currency`;
      }
      return el;
    });
  }
  return ctx.columnKeys.join(',');
};

helpers.groupBy = (ctx) => {
  const columnCache = cloneDeep(ctx.columnKeys);
  if (ctx.valueField !== 'usd' && ctx.valueField) {
    const currencyColumnIdx = columnCache.indexOf('currency');
    columnCache.splice(currencyColumnIdx, 1);
    columnCache.push(currencyMap[ctx.valueField]);
  }
  return `GROUP BY ${columnCache.join(', ')}`;
};

helpers.sort = (ctx) => {
  const sortQuery = ctx.columnKeys
                      .filter(key => key !== ctx.sortedColumnId)
                      .map((key, i) => {
                        return `${key} ASC`;
                      });
  if (ctx.sortedColumnId && ctx.sortDirection) {
    sortQuery.unshift(`${ctx.sortedColumnId} ${ctx.sortDirection}`);
  }

  return `ORDER BY ${sortQuery.join(', ')}`;
};

helpers.limit = (ctx) => {
  const offset = ctx.page ? `${ (ctx.pageSize * ctx.page) - ctx.pageSize }` : '0';
  return `LIMIT ${offset}, ${ctx.pageSize}`;
};

helpers.combinedWhere = (ctx) => {
  const clauses = [];
  if (ctx.columnFilters && Object.keys(ctx.columnFilters).length) {
    _forEach(ctx.columnFilters, (val, key) => {
      const safeFilters = makeSQLSafeAgain(val);
      clauses.push(`${key} IN ('${safeFilters.join('\',\'')}')`);
    });
  }
  return `WHERE ${clauses.join(' AND ')}`;
};

helpers.where = (ctx, table, complex = false, withRange = true) => {
  const clauses = [];
  if (ctx.compositeFilters && Object.keys(ctx.compositeFilters).length) {
    _forEach(ctx.compositeFilters, (val) => {
      const orClauses = [];
      _forEach(val, (inVal, inKey) => {
        const safeFilters = makeSQLSafeAgain(inVal);
        orClauses.push(`${inKey} IN ('${safeFilters.join('\',\'')}')`);
      });
      clauses.push(`(${orClauses.join(' OR ')})`);
    });
  }

  if (ctx.filters && Object.keys(ctx.filters).length) {
    _forEach(ctx.filters, (val, key) => {
      const safeFilters = makeSQLSafeAgain(val);
      clauses.push(`${key} IN ('${safeFilters.join('\',\'')}')`);
    });
  }

  if (complex && table.toLowerCase().indexOf('complex') > -1) {
    let allColumns = [];
    _forEach(mappings, (val, key) => {
      if (val.nullable) {
        allColumns = allColumns.concat(val.nullableColumns);
      }
    });

    const nonspecific = _difference(allColumns, ctx.columnKeys);
    nonspecific.forEach((col) => {
      clauses.push(`(${col} IS NULL OR ${col} = '')`);
    });
  }

  // Set the range
  if (withRange) {
    const start = new Date(ctx.range.start);
    start.setDate(start.getDate());
    const end = new Date(ctx.range.end);
    clauses.push(`(Month >= '${start.toISOString().slice(0, 19).replace('T', ' ')}'
                  AND Month <= '${end.toISOString().slice(0, 19).replace('T', ' ')}')`);
  }

  // Subscriptions
  if (ctx.subscriptions && ctx.subscriptions.length) {
    const subscriptionsQuery =
      ctx.subscriptions
        .map(sub => `subscriptionid LIKE '%${sub}%'`);
    clauses.push(`(${subscriptionsQuery.join(' OR ')})`);
  }

  return `WHERE ${clauses.join(' AND ')}`;
};

helpers.whereDataset = (ctx) => {
  let subscriptionsQuery = [];
  if (ctx.subscriptions && ctx.subscriptions.length) {
    subscriptionsQuery = ctx.subscriptions.map(sub => `subscriptionid LIKE '%${sub}%'`);
  }

  return `WHERE (${subscriptionsQuery.join(' OR ')}) AND (recordstatus NOT LIKE 'Deleted')`;
};

const buildLikeRecordStatus = ctx => {
  if (!(ctx.recordstatus && Array.isArray(ctx.recordstatus))) {
    return '';
  }

  const likes = ctx.recordstatus.map(recordstatus => `recordstatus like '${recordstatus}'`);
  return `AND ( ${likes.join(' OR ')} )`;
};

const buildCompareDateStart = ctx => {
  return ctx.lastupdateddatestart
    ? `AND (lastupdateddate >= '${ctx.lastupdateddatestart}')`
    : '';
};

const buildCompareDateEnd = ctx => {
  return ctx.lastupdateddateend
    ? `AND (lastupdateddate <= '${ctx.lastupdateddateend}')`
    : '';
};

helpers.pivot = (ctx, aggFn, columnParam) => {
  const start = new Date(ctx.range.start);
  const end = new Date(ctx.range.end);
  const getHeaders = ctx.range.interval === 'yearly' ? headerGenerator.getYearlyHeaders : headerGenerator.getQuarterlyHeaders;
  const headers = getHeaders(start, end).map(header => header.key);
  const column = ![undefined, 'usd'].includes(ctx.valueField) && aggFn === 'SUM' ? ctx.valueField : columnParam;

  const pivotQuery = headers.map((header) => {
    let asHeader = header;
    if (aggFn !== 'SUM') {
      asHeader = `${aggFn.toLowerCase()}_${asHeader}`;
    }
    const splitHeaderDate = header.split('/').reverse();
    return (ctx.recordstatus && Array.isArray(ctx.recordstatus)) || ctx.lastupdateddatestart || ctx.lastupdateddateend
      ? `${aggFn}(CASE WHEN Month = '${splitHeaderDate.join('')}' ${buildLikeRecordStatus(ctx)} ${buildCompareDateStart(ctx)} ${buildCompareDateEnd(ctx)} THEN ${column} ELSE NULL END) AS '${asHeader}'`
      : `${aggFn}(CASE WHEN Month = '${splitHeaderDate.join('')}' AND recordstatus NOT LIKE \'Deleted\' THEN ${column} ELSE NULL END) AS '${asHeader}'`;
  });

  return pivotQuery.join(`,\n`);
};

helpers.groupConcat = (ctx, column) => {
  const start = new Date(ctx.range.start);
  const end = new Date(ctx.range.end);
  const getHeaders = ctx.range.interval === 'yearly' ? headerGenerator.getYearlyHeaders : headerGenerator.getQuarterlyHeaders;
  const headers = getHeaders(start, end).map(header => header.key);

  const pivotQuery = headers.map((header) => {
    const splitHeaderDate = header.split('/').reverse();
    return (ctx.recordstatus && Array.isArray(ctx.recordstatus)) || ctx.lastupdateddatestart || ctx.lastupdateddateend
      ? `GROUP_CONCAT(CASE WHEN Month = '${splitHeaderDate.join('')}' ${buildLikeRecordStatus(ctx)} ${buildCompareDateStart(ctx)} ${buildCompareDateEnd(ctx)} THEN ${column} ELSE NULL END) AS '${column}_${header}'`
      : `GROUP_CONCAT(CASE WHEN Month = '${splitHeaderDate.join('')}' THEN ${column} ELSE NULL END) AS '${column}_${header}'`;
  });

  return pivotQuery.join(`,\n`);
};

const makeSQLSafeAgain = (filters) => {
  if (filters && filters.length) {
    filters = filters.map((filter) => {
      return sqlEscape(filter);
    });
  }

  return filters;
}

const sqlEscape = (str) => {
  const regex = new RegExp(/[\0\x08\x09\x1a\n\r"'\\]/g);
  const escaper = (char) => {
    const m = ['\\0', '\\x08', '\\x09', '\\x1a', '\\n', '\\r', "'", '"', "\\", '\\\\'];
    const r = ['\\\\0', '\\\\b', '\\\\t', '\\\\z', '\\\\n', '\\\\r', "''", '""', '\\\\', '\\\\\\\\'];
    return r[m.indexOf(char)];
  }

  return str.replace(regex, escaper);
}