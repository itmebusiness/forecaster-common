const hbsHelpers = require('./hbsHelpers');
const _forEach = require('lodash/forEach');

module.exports = function helpers(Handlebars) {
  const hbs = Handlebars || require('handlebars');
  _forEach(hbsHelpers, (val, key) => {
    hbs.registerHelper(key, val);
  });

  return hbs.helpers;
}

