const Stream = require('stream');
const sourceTypeMap = require('./sourceTypeMap.json');

class TransformStream extends Stream.Transform {
    constructor() {
        super({
            objectMode: true
        });
        this.defaultAvgColour = sourceTypeMap['mixed'].name;
    }

    _transform(obj, encoding, callback) {
        obj.avgColour = sourceTypeMap[obj.source_average] ? sourceTypeMap[obj.source_average].name : this.defaultAvgColour;

        Object.keys(obj).forEach(key => {
            if(key.indexOf('avg_') > -1) {
                obj[key] = sourceTypeMap[obj[key]] ? sourceTypeMap[obj[key]].name :  this.defaultAvgColour;
            }
        });
        
        this.push(obj);
        callback();
    }
}

module.exports = () => {
    return new TransformStream();
};