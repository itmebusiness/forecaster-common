const mysql = require('mysql');
const bunyan = require('./logger');
const os = require('os');
const Stream = require('stream');

class SQLStream extends Stream.Readable {
  constructor(options, pool) {
    super({
      objectMode: true
    });

    if (!this._logger) {
        this._logger = bunyan(options.logger);
    }

    this.options = options;
    this._pool = pool;
  }

  _read() {}

  connect() {
    const ctx = this;
    return new Promise((resolve, reject) => {
      if (ctx.pool) {
        ctx.pool.getConnection((err, connection) => {
          if (err) {
            return reject(err);
          }

          console.log('[MemSQL] Got Connection');
          ctx._connection = connection;
          // Set up the event listeners
          ctx.setupEvents();

          return resolve(connection);
        });
      }
      else {
        reject(new Error('The connection pool is not initialised'));
      }
    });
  }

  setupEvents() {
    this.connection.on('error', this.onError.bind(this));
    console.log('[MemSQL] Event listeners initialised');
  }

  query(params) {
    this.query = this.connection.query(params);
    this.query.on('result', this.onResult.bind(this));
    this.query.on('end', this.onEnd.bind(this));
    this.query.on('error', this.onError.bind(this));
  }

  onResult(row) {
    this.push(row);
  }

  onEnd() {
    this.connection.release();
    this.push(null);
  }

  onError(err) {
    console.log(err);
    this.connection.release();
    this.logger.error(err);
  }

  get logger() {
    return this._logger;
  }

  get pool() {
    return this._pool;
  }

  get connection() {
    return this._connection;
  }
}

module.exports = SQLStream;