const os = require('os');
const bunyan = require('../../lib/logger');

const format = value => value.toString();
const date = new Date();
const yyyy = date.getUTCFullYear();
const mm = format(date.getUTCMonth() + 1);
const dd = format(date.getUTCDate());

module.exports = options => {
  return bunyan({
    level: options.logLevel,
    cloudwatch: options.cloudwatch && {
        ...options.cloudwatch,
        logStreamName: `${yyyy}/${mm}/${dd}/${os.hostname()}`
    }
  });
}