
const range = require('lodash/range');


const zeroFill = (number, digits) => number.toString().padStart(digits, '0');
const dateKey = date => `${zeroFill(date.getUTCDate(), 2)}/${zeroFill(date.getUTCMonth() + 1, 2)}/${date.getUTCFullYear()}`;
const yearKey = year => dateKey(new Date(Date.UTC(year + 1, 0, 0)));

module.exports.getYearlyHeaders = (start, end) => {
    const startYear = new Date(start).getUTCFullYear();
    const endYear = new Date(end).getUTCFullYear();
    return range(startYear, endYear + 1).map(year => ({
        key: yearKey(year),
        label: year.toString()
    }));
};


const quarterLabel = index => ['Q1', 'Q2', 'Q3', 'Q4'][index];
const quarterKey = (year, quarter) => dateKey(new Date(Date.UTC(year, (quarter + 1) * 3, 0)));

module.exports.getQuarterlyHeaders = (start, end) => {
    const startYear = new Date(start).getUTCFullYear();
    const startQuarter = Math.floor(new Date(start).getUTCMonth() / 3);
    const endYear = new Date(end).getUTCFullYear();
    const endQuarter = Math.floor(new Date(end).getUTCMonth() / 3);
    const l = (endYear - startYear) * 4 + endQuarter - startQuarter + 1;
    return range(0, l).map(i => {
        let year = startYear + Math.floor((startQuarter + i) / 4);
        let quarter = (startQuarter + i) % 4;
        return {
            key: quarterKey(year, quarter),
            label: `${quarterLabel(quarter)} ${year}`
        }
    });
};