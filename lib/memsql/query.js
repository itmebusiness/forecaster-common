const QueryGenerator = require('./decorators/mssqlTemplate');
const mappings = require('./mappings.json');
const { 
  sourceTemplate,
  subSelectTemplate,
  sourceTemplateCount,
  distinctColumnTemplate,
  filterWhereTemplate,
  filterWhereTemplateCount
} = require('./decorators/templates');

function convertFields(filtersObject) {  
  Object.keys(filtersObject).forEach(filter => {
    if (mappings[filter].columns) {
      mappings[filter].columns.forEach(subFilter => {
        filtersObject[subFilter] = filtersObject[filter];
      });

      delete filtersObject[filter];
    }
  });
}
  
function convertCompositeFields(filtersObject) {
  return Object.keys(filtersObject).reduce((result, filterGroup) => {
    if(mappings[filterGroup].columns) {
      result[filterGroup] = mappings[filterGroup].columns.reduce((group, filter) => {
        group[filter] = filtersObject[filterGroup];
        return group;
      }, {});
    }
    return result;
  }, {});
}

module.exports = (requestQuery, pageSize = 200, countOnly = false, filterDistinct = false, filteredQuery = false) => {
  convertFields(requestQuery.filters);
  requestQuery.compositeFilters = convertCompositeFields(requestQuery.compositeFilters);
  const qGen = new QueryGenerator(requestQuery, pageSize);
  if(filteredQuery) {
    qGen.sourceTemplateContent = (countOnly ? filterWhereTemplateCount : filterWhereTemplate);
  }
  else if (!filterDistinct) {
    qGen.sourceTemplateContent = (countOnly ? sourceTemplateCount : sourceTemplate);
  }
  else {
    qGen.sourceTemplateContent = distinctColumnTemplate;
  }
  qGen.subSelectTemplateContent = subSelectTemplate;
  
  return qGen.query;
};