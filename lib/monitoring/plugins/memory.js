const memwatch = require('@airbnb/node-memwatch');

class MemoryPlugin {
    constructor(monitoringOptions, pluginOptions, cloudwatch) {
        this.monitoringOptions = monitoringOptions;
        this.pluginOptions = pluginOptions;
        this.cloudwatch = cloudwatch;
        this._start();
    }

    _start() {
        memwatch.on('stats', stats => {
            console.log('stats', stats);
            this.cloudwatch.recordMetric({
                MetricName: 'MEMORY_USAGE',
                Unit: 'Bytes',
                Value: stats.total_heap_size
            });
        });
    }
}

module.exports = MemoryPlugin;