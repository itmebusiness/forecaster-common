const AWS = require('aws-sdk');


const sample = {
    MetricName: 'RESPONSE_TIME',
    Dimension: [
        {Name: 'COMPONENT', value: 'DATA_API'},
        {Name: 'REQUEST_TYPE', value: 'QUERY'}
    ],
    Value: '20'
};


class Monitoring {
    constructor(options) {
        this.options = options;
        this.cloudwatch = new AWS.CloudWatch({
            region: 'eu-west-1'
        });
        this.plugins = [];
        if (this.options && this.options.plugins) {
            this.processPlugins();
        }

    }

    getDefaultPararms() {
        return {
            Namespace: 'TMT'
        }
    }

    getDefaultDimensions() {
        return [
            {Name: 'ENVIRONMENT', Value: this.options.env},
            {Name: 'COMPONENT', Value: this.options.componentName}
        ]
    }

    recordMetric(params) {
        //If monitoring is configured
        if (this.options) {
            const cloudwatchParams = this.getDefaultPararms();
            cloudwatchParams.MetricData = [params];
            if(params.Dimensions) {
                params.Dimensions = this.getDefaultDimensions().concat(params.Dimensions);
            }
            else {
                params.Dimensions = this.getDefaultDimensions();
            }

            this.cloudwatch.putMetricData(cloudwatchParams, (err, data) => {
                console.log(err);
                console.log(data);
            });
        }
    }

    processPlugins() {
        this.options.plugins.forEach(plugin => {
            const p = require(`./plugins/${plugin.name}.js`);
            this.plugins.push(new p(this.options, plugin, this));
        });
    }
}

module.exports = options => {
    if (!global._monitoring) {
        global._monitoring = new Monitoring(options);
    }
    return global._monitoring;
};