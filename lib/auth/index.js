'use strict';

const request = require('request');
const jwt = require('jsonwebtoken');
const jwksRsa = require('jwks-rsa');

const ENTITLEMENTS_KEY = 'https://ovumforecaster.com/entitlements';

const JwksClient = config => {

  const validator = (config.jwt || jwt);
  const verifyAccessTokenUsingKey = (token, key, cb) =>
    validator.verify(token, key, {
      issuer: config.allowedIssuers,
      audience: config.audience,
      algorithms: ['RS256']
    }, cb);
  const client = (config.jwks || jwksRsa)({
    strictSsl: true,
    cache: true,
    //cacheMaxEntries: 5,
    //cacheAge: 36000,
    jwksUri: config.jwksUri
  });

  return {
    verifyAccessToken: token => new Promise((resolve, reject) => {

      // To verify the token comprehensively we must first
      // find which key was used to sign the token, this information
      // is stored in the JWT header.
      const keyId = tokenKeyId(
        validateAlgorithm(
          decodeTokenIncludingHeader(validator, token),
          ['RS256']));

      // We need to find a matching key in the JWKS (the known
      // key-set for the API).
      client.getSigningKey(keyId, (err, key) => {
        if (err) {
          reject(new Error('token signed with unknown key'));
        } else {
          const signingKey = key.publicKey || key.rsaPublicKey;

          // And then verify the token using the signing key
          verifyAccessTokenUsingKey(token, signingKey, (err, verified) => {
            if (err) {
              reject(new Error('invalid token'));
            } else {
              resolve(verified);
            }
          });
        }
      });
    })
  }
};

const validateAlgorithm = (decoded, algorithms) => {
  if (!algorithms.includes(decoded.header.alg)) {
    throw new Error('token has invalid algorithm');
  }
  return decoded;
};

const tokenKeyId = (decoded) =>
  decoded.header.kid;

const getEntitlements = user =>
  user[ENTITLEMENTS_KEY] || [];

const Auth0Client = config => {
  const fetch = (config.request || request).get;
  return {
    fetchUserInfo: token => new Promise((resolve, reject) => {
      fetch({
        url: `${config.auth0Uri}/userinfo`,
        method: 'GET',
        gzip: true,
        json: true,
        headers: {
          'connection'    : 'keep-alive',
          'content-type'  : 'application/json; charset=UTF-8',
          'authorization' : `Bearer ${token}`
        }
      }, (err, res) => {
        if (err) {
          reject(new Error('unexpected error'));
        } else if (res.statusCode !== 200) {
          reject(new Error(`unexpected ${res.statusCode} response`));
        } else if (!res.body) {
          reject(new Error('missing response body'));
        } else {
          resolve(res.body);
        }
      });
    })
  };
};

const AuthClient = config => {
  const jwksClient = JwksClient(config);
  const auth0Client = Auth0Client(config);
  return {

    // API access token gives us scopes but tells us next to
    // nothing about the actual user, but does tell us about
    // the scopes of access this token affords to the bearer.
    verifyAccessToken: token =>

      // Verify the token is a valid token and check the token
      // scopes / permissions.
      jwksClient.verifyAccessToken(token)
        .then(verifyScopes(config.requiredScopes)),

    fetchUserInfo: token =>

      // To discover the users entitlements we need to request
      // the userinfo associated with the token.
      auth0Client
        .fetchUserInfo(token)
  };
};

const authFlow = (client, token) =>
  client.verifyAccessToken(token);

const AuthGateway = config => {

  const validator = (config.jwt || jwt);

  const authClient = AuthClient({
    jwt: validator,
    jwks: config.jwks,
    request: config.request,
    auth0Uri: config.auth0Uri,
    jwksUri: config.jwksUri,
    allowedIssuers: config.allowedIssuers,
    audience: config.audience,
    requiredScopes: config.requiredScopes
  });

  const auth = token => authFlow(authClient, token);

  return {
    auth,
    authEvent: event => Promise.resolve(event)
      .then(getBearerTokenFromEvent)
      .then(auth)
  };
};

const tokenScopes = token =>
  token.scope ? token.scope.split(' ') : [];

const containsScopes = (grantedScopes, requiredScopes) =>
  requiredScopes.every(scope => grantedScopes.includes(scope));

const missingScopes = (grantedScopes, requiredScopes) =>
  requiredScopes.filter(scope => !grantedScopes.includes(scope));

const verifyScopes = requiredScopes => token => {
  const grantedScopes = tokenScopes(token);
  if (!containsScopes(grantedScopes, requiredScopes)) {
    throw new Error(`missing required scopes: ${missingScopes(grantedScopes, requiredScopes).join(' ')}`);
  }
  return token;
};

const decodeTokenIncludingHeader = (validator, token) => {
  const decoded = validator.decode(token, { complete: true });
  if (!decoded) {
    throw new Error('invalid token');
  }
  return decoded;
};

const getBearerTokenFromEvent = event => {
  // Fix for inconsistent fetch api implementation throughout different browsers and versions
  // C.f. https://fetch.spec.whatwg.org/#terminology-headers
  const authHeader = event.headers['Authorization'] || event.headers['authorization'];
  if (!authHeader) {
    throw new Error('missing authorization header');
  }
  return authHeader.substring(7);
};

module.exports = {
  JwksClient,
  Auth0Client,
  AuthGateway,
  getEntitlements,
  verifyScopes,
};
