const bunyan = require('bunyan');
const bunyanCloudwatch = require('bunyan-cloudwatch');

module.exports = config =>
    bunyan.createLogger({
        name: 'forecaster',
        level: config.level,
        streams: [{
            stream: process.stdout
        }].concat(config.cloudwatch ? {
            stream: bunyanCloudwatch(config.cloudwatch),
            type: 'raw'
        } : [])
    });
