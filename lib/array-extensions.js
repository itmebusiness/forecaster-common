function range(start, end) {
  const count = end - start + 1;

  if (count < 0)
    return [];

  return Array.apply(0, Array(count)).map((element, index) => index + start);
}

module.exports = range;
