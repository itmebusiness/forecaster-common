const {expect} = require('chai');
const {spy, stub} = require('sinon');
const proxyquire = require('proxyquire').noPreserveCache().noCallThru();

class CloudWatch {
    constructor() {
    }

    putMetricData(args) {

    }
}

const fakeAWS = {
    CloudWatch: CloudWatch
};

class fakePlugin {

}


let monitoring;
const defaultConfig = {
    "env": "dev",
    "componentName": "FORECASTER_API"
};


function createMonitoring(config) {
    delete global._monitoring;
    monitoring = proxyquire('../../lib/monitoring', {
        'aws-sdk': fakeAWS,
        './plugins/fake.js': fakePlugin
    })(config);
}


describe('Monitoring', () => {

    describe('Initialisation', () => {
        it('should create a cloudwatch reference', () => {
            createMonitoring(defaultConfig);
            expect(monitoring.cloudwatch instanceof CloudWatch).to.equal(true);
        });
    });

    describe('recordMetric', () => {

        beforeEach(() => {
            createMonitoring(defaultConfig);
        });

        it('should no do anything if no monitoring options sepcified', () => {
            createMonitoring(undefined);
            const defaultParamsSpy = spy(monitoring, 'getDefaultPararms');
            monitoring.recordMetric({});
            expect(defaultParamsSpy.called).to.equal(false);
        });

        it('should combine default dimensions with those provided', () => {
            const recordMetricSpy = spy(monitoring.cloudwatch, 'putMetricData');
            monitoring.recordMetric({
                MetricName: 'TEST_METRIC',
                Unit: 'Bytes',
                Value: 10
            });

            expect(recordMetricSpy.args[0][0]).to.deep.equal({
                Namespace: 'TMT',
                MetricData: [{
                    MetricName: 'TEST_METRIC',
                    Unit: 'Bytes',
                    Value: 10,
                    Dimensions: [
                        {Name: 'ENVIRONMENT', Value: "dev"},
                        {Name: 'COMPONENT', Value: "FORECASTER_API"}
                    ]
                }]
            });
        });
    });


    describe('Plugins', () => {
        beforeEach(() => {
            createMonitoring({
                "env": "dev",
                "componentName": "FORECASTER_API",
                "plugins": [
                    {
                        "name": "fake"
                    }
                ]
            });
        });

        it('should create plugin', () => {
            expect(monitoring.plugins.length).to.equal(1);
            expect(monitoring.plugins[0] instanceof fakePlugin).to.equal(true);
        });

        it('should not error if no plugins specified', () => {
            createMonitoring({
                "env": "dev",
                "componentName": "FORECASTER_API",
                "plugins": []
            });
            expect(monitoring.plugins.length).to.equal(0);
        });
    });


});