const { expect } = require('chai');
const { stub } = require('sinon');
const authorizer = require('../middleware/authoriser');

describe('Authorizer', () => {

    let auth;

    beforeEach(() => {
        stub(authorizer, 'AuthGateway').returns({
            auth: token => Promise.resolve(auth)
        });
    });

    afterEach(() => {
        authorizer.AuthGateway.restore();
    });

    it('parses subscriptions', done => {
        auth = {
            'http://example.com/account': {
                subscriptions: [
                    'PT0101-1',
                    'PT0102-2'
                ]
            }
        };
        let req = {
            get: name => 'value'
        };
        authorizer({
            auth: {
                namespace: 'http://example.com'
            }
        })(req, {}, (err) => {
            if (err) {
                done(err);
            } else {
                expect(req.auth.subscriptions).to.deep.equal([
                    { value: 'PT0101-1', dataset: 'PT0101', trial: false },
                    { value: 'PT0102-2', dataset: 'PT0102', trial: true }
                ]);
                done();
            }
        });
    });

    it('handles garbage subscriptions', done => {
        auth = {
            'http://example.com/account': {
                subscriptions: [
                    'PT0101-1',
                    'XX0101-1',
                    'garbage'
                ]
            }
        }
        let req = {
            get: name => 'value'
        };
        authorizer({
            auth: {
                namespace: 'http://example.com'
            }
        })(req, {}, (err) => {
            if (err) {
                done(err);
            } else {
                expect(req.auth.subscriptions).to.deep.equal([
                    { value: 'PT0101-1', dataset: 'PT0101', trial: false }
                ]);
                done();
            }
        });
    });

    it('rejects user with only garbage subscriptions', done => {
        auth = {
            'http://example.com/account': {
                subscriptions: [
                    'XX0101-1',
                    'garbage'
                ]
            }
        }
        let req = {
            get: name => 'value'
        };
        authorizer({
            auth: {
                namespace: 'http://example.com'
            }
        })(req, {}, (err) => {
            expect(err.code).to.equal('ENOTAUTHORIZED');
            done();
        });
    });
});
